//
//  FirebaseAPITests.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Quick
import Nimble
import FirebaseAuth
import Firebase

@testable import SDOSTest

class FirebaseAPITests: QuickSpec {
    override func spec() {
        beforeSuite {
            if FirebaseApp.app() == nil {
                FirebaseApp.configure()
            }
            waitUntil(timeout: 3, action: { (action) in
                Auth.auth()
                    .signIn(withEmail: "admin@gmail.com", password: "adminPassword",
                            completion: { (authData, error) in
                                if let auth = authData {
                                    print("\(auth.user)")
                                }
                                action()
                    })
            })
        }
        describe("Firebase API") {
            it("Get user", closure: {
                let admin = "admin@gmail.com"
                waitUntil(action: { (action) in
                    firebaseApiProvider
                        .requestObject(XUser.self, target: .getUser(admin)) { (response) in
                            switch response {
                            case .error:
                                fail()
                            case .success(let user):
                                expect(user).notTo(beNil())
                            }
                            action()
                    }
                })
            })
            it("Get tasks", closure: {
                waitUntil(action: { (action) in
                    firebaseApiProvider
                        .requestArray(Task.self, .getTasks("tecnic@gmail.com")) { (response) in
                            switch response {
                            case .error: fail()
                            case .success(let tasks):
                                expect(tasks.count).notTo(be(0))
                                expect(tasks.count).to(be(1))
                            }
                            action()
                    }
                })
            })
            it("Create new task", closure: {
                waitUntil(action: { (action) in
                    firebaseApiProvider
                        .request(.createNewTask(Task.random)) { (response) in
                            switch response {
                            case .failure: fail()
                            case .success(let value):
                                expect(value.isCommitted).to(beTrue())
                            }
                            action()
                    }
                })
            })
            it("Update task", closure: {
                waitUntil(action: { (action) in
                    var task = Task.random
                    task.state = (task.state == .finished) ? .open:.finished
                    firebaseApiProvider
                        .request(.updateTask(task)) { (response) in
                            switch response {
                            case .failure: fail()
                            case .success(let value):
                                expect(value.isCommitted).to(beTrue())
                            }
                            action()
                    }
                })
            })
            it("Update user") {
                waitUntil(timeout: 2) { action in
                    firebaseApiProvider
                        .request(.updateUserInfo(XUser.testUser)) { (response) in
                            switch response {
                            case .failure: fail()
                            case .success(let value):
                                expect(value.isCommitted).to(beTrue())
                            }
                            action()
                    }
                }
            }
        }
    }
}

extension Task {
    static var random: Task {
        return Task(JSON: [
            "description": "Test task",
            "state": "finished",
            "time": 1,
            "type": "pay",
//            "id": "FDBBDddasE41-48F8-4F45-87E8-42B6BEF924BB636706246189666048",
            "userId": "tecnic@gmail.com"
            ])!
    }
}
extension XUser {
    static var testUser: XUser {
        return XUser(JSON: [
            "email" : "tecnic1@gmail.com",
            "id" : "tecnic1",
            "name" : "tecnic1",
            "role" : "tecnic",
            "tasksNumber" : randomNumber(inRange: 1...10),
            "totalTimeAssigned" : Double(randomNumber(inRange: 10...100))
            ])!
    }
}
public func randomNumber<T : SignedInteger>(inRange range: ClosedRange<T> = 1...6) -> T {
    let length = Int64(range.upperBound - range.lowerBound + 1)
    let value = Int64(arc4random()) % length + Int64(range.lowerBound)
    return T(value)
}
