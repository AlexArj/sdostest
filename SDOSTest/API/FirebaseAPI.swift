//
//  FirebaseAPI.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import Nora
import FirebaseDatabase
import Firebase
import ObjectMapper

let firebaseApiProvider = NRDatabaseProvider<APIFirebase>()

enum APIFirebase {
    case getUser(String)
    case getTasks(String)
    case createNewTask(Task)
    case updateTask(Task)
    case getUsers
    case updateUserInfo(XUser)
}

extension APIFirebase: NRDatabaseTarget {
    var baseReference: DatabaseReference {
        return Database.database().reference()
    }
    var path: String {
        switch self {
        case .getUser, .getUsers:
            return "users/"
        case .getTasks, .createNewTask:
            return "tasks/"
        case .updateTask(let task):
            return "tasks/\(task.id ?? "")"
        case .updateUserInfo(let user):
            return "users/\(user.id ?? "")"
        }
    }
    var task: NRDatabaseTask {
        switch self {
        case .getUser, .getUsers: return .observeOnce(.value)
        case .getTasks: return .observe(.value)
        case .createNewTask(let task):
            let value = [(task.id ?? ""):task.toJSON()]
            return .updateChildValues(value)
        case .updateTask(let task):
            return .updateChildValues(task.toJSON())
        case .updateUserInfo(let user):
            return .updateChildValues(user.toJSON())
        }
    }
    var queries: [NRDatabaseQuery]? {
        switch self {
        case .getUser(let userId):
            return [NRDatabaseQuery.orderedByChild(key: "id"),
                    NRDatabaseQuery.equalToValue(userId)]
        case .getTasks(let userId):
            return [NRDatabaseQuery.orderedByChild(key: "userId"),
                    NRDatabaseQuery.equalToValue(userId)]
        case .updateTask(let task):
            return [NRDatabaseQuery.orderedByChild(key: "id"),
                    NRDatabaseQuery.equalToValue(task.id)]
        default: return nil
        }
    }
}
extension APIFirebase {
    static func updateUser(_ task: Task, user: XUser, completion: @escaping (DefaultResponse) -> Void) {
        let isTaskOpen: (Bool) = (task.state == .open)
        var user = user
        user.tasksNumber += isTaskOpen ? 1: -1
        let taskTime = (task.time ?? 0)
        user.totalTimeAssigned += isTaskOpen ? taskTime: -taskTime
        firebaseApiProvider.request(.updateUserInfo(user)) { (result) in
            switch result {
            case .failure(let error): completion(.error(error))
            case .success:
                userState.value = .loggedIn(user)
                completion(.success)
            }
        }
    }
}
