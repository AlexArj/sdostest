//
//  UIView+Extension.swift
//  SocialDrone
//
//  Created by Alejandro Arjonilla Garcia on 04.05.18.
//  Copyright © 2018 socialdrone. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    /// SwifterSwift: Corner radius of view; also inspectable from Storyboard.
    @IBInspectable public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.masksToBounds = true
            layer.cornerRadius = abs(CGFloat(Int(newValue * 100)) / 100)
        }
    }
}
extension UIView {
    func showAnimated() {
        guard alpha == 0 else { return }
        UIView.animate(withDuration: 0.5) {
            self.alpha = 1
        }
    }
    func hideAnimated() {
        DispatchQueue.main.async {
            guard self.alpha == 1 else { return }
            UIView.animate(withDuration: 0.5, animations: {
                self.alpha = 0
            }) { (_) in
                self.removeFromSuperview()
            }
        }
    }
}

extension UIView {
    
    // In order to create computed properties for extensions, we need a key to
    // store and access the stored property
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    // Set our computed property type to a closure
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    // This is the meat of the sauce, here we create the tap gesture recognizer and
    // store the closure the user passed to us in the associated object we declared above
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // Every time the user taps on the UIImageView, this function gets called,
    // which triggers the closure we stored
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    func dismissFadeOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }, completion: { (finished) in
            if finished {
                self.removeFromSuperview()
            }
        })
    }
}
