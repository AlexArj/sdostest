//
//  UIViewController+BaseView.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 24.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import UIKit
import PMAlertController
extension UIViewController: BaseView {
    internal var loadingView: LoadingView? {
        return self.view.subviews
            .map { $0 as? LoadingView }
            .compactMap { $0 }
            .first
    }

    func addLoadingView() {
        guard loadingView == nil else { return }
        DispatchQueue.main.async {
            let baseView: UIView = self.view
            let deviceBounds = UIScreen.main.bounds
            let view = LoadingView(frame: deviceBounds)
            baseView.addSubview(view)
            self.loadingView?.showAnimated()
        }
    }
    func removeLoadingView() {
        let view = loadingView
        view?.hideAnimated()
    }
    func showAlert(with title: String, description: String, image: UIImage? = #imageLiteral(resourceName: "sdosIcon"), actions: [AlertOption]) {
        removeLoadingView()
        let alert = PMAlertController(title: title, description: description, image: image, style: .walkthrough)
        
        actions.forEach { action in
            alert.addAction(action.pmAlertAction)
        }
        self.present(alert, animated: true, completion: nil)
    }
    func showError(with error: Error) {
        let actions: [AlertOption] = [
           AlertOption(title: "Ok", action: {})
        ]
        showAlert(with: "Ups!", description: error.localizedDescription, image: #imageLiteral(resourceName: "errorIcon"), actions: actions)
    }
}
extension AlertOption {
    var pmAlertAction: PMAlertAction {
        return PMAlertAction(title: self.title, style: .default, action: { self.action() })
    }
}
