//
//  Date+Extension.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}
