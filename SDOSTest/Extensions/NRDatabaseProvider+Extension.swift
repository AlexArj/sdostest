//
//  NRDatabaseProvider+Extension.swift
//  TabelRentPlatform
//
//  Created by Alejandro Arjonilla Garcia on 29.05.18.
//  Copyright © 2018 Tabel. All rights reserved.
//

import Foundation
import Nora
import ObjectMapper

public enum Response<T> {
    case success(T)
    case error(Error)
}
enum DefaultResponse {
    case success
    case error(Error)
}
enum ResponseError: Error {
    case unhandledError
    case parsingError
    case incorrectData
    case NoUserFound
    
    var localizedDescription: String {
        switch self {
        case .unhandledError:
            return "Unhandled Error"
        case .parsingError:
            return "Parsing Error"
        case .incorrectData:
            return "Incorrect data, please type correct data"
        case .NoUserFound:
            return "No user found"
        }
    }
}
extension NRDatabaseProvider {
    public func requestArray<T: Mappable>(_ type: T.Type, _ target: Target,
                                          completion: @escaping (Response<[T]>) -> Void) {
        self.request(target) { response in
            switch response {
            case .success(let value):
                guard let jsonValue = try? value.childrenAsJSON() else {
                    completion(.success([]))
                    return
                }
                let values = Mapper<T>().mapArray(JSONArray: jsonValue)
                completion(.success(values))
            case .failure(let error): completion(.error(error))
            }
        }
    }
    public func requestObject<T: Mappable>(_ type: T.Type,
                                           target: Target,
                                           completion: @escaping (Response<T>) -> Void) {
        self.request(target) { response in
            switch response {
            case .success(let value):
                guard let jsonValue = value.json?.first?.value as? [String: Any] else {
                    completion(.error(ResponseError.unhandledError))
                    return
                }
                let values = T(JSON: jsonValue)
                completion(.success(values!))
            case .failure(let error): completion(.error(error))
            }
        }
    }
}
