//
//  UITextField+Extension.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 24.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func addDoneButtonOnKeyboard(with title: String = "Done", action: (() -> Void)? = nil)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIButton()
        doneButton.setTitle(title, for: .normal)
        doneButton.setTitleColor(#colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1), for: .normal)
        doneButton.add(for: .touchUpInside) {
            action?()
            self.resignFirstResponder()
        }
        let done: UIBarButtonItem = UIBarButtonItem(customView: doneButton)
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
}
extension UITextView {
    func addDoneButtonOnKeyboard(with action: (() -> Void)? = nil)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIButton()
        doneButton.setTitle("Done", for: .normal)
        doneButton.setTitleColor(#colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1), for: .normal)
        doneButton.add(for: .touchUpInside) {
            action?()
            self.resignFirstResponder()
        }
        let done: UIBarButtonItem = UIBarButtonItem(customView: doneButton)
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
}
