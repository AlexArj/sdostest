//
//  NavBarStyle.swift
//  SocialDrone
//
//  Created by Alejandro Arjonilla Garcia on 07.05.18.
//  Copyright © 2018 socialdrone. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    func styleMe() {
        self.isTranslucent = false
        self.barTintColor = .white
        self.tintColor = colorStyle.red
        self.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: colorStyle.red,
            NSAttributedStringKey.font: UIFont(name: "Helvetica", size: 17)
        ]

    }
}
extension UINavigationController {
    static var grayNavController: UINavigationController {
        let controller = UINavigationController()
        controller.navigationBar.styleMe()
        return controller
    }
}
