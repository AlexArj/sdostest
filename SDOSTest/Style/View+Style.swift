//
//  View+Style.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 24.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func setTitleStyle() {
        self.font = UIFont(name: "Helvetica", size: 20)
        self.textColor = colorStyle.black
    }
    func setSubTitleStyle() {
        self.font = UIFont(name: "Helvetica", size: 16)
        self.textColor = colorStyle.gray
    }
}
protocol Style {
    func style()
}
extension UITextField: Style {
    func style() {
        self.font = UIFont(name: "Helvetica", size: 16)
        self.textColor = colorStyle.gray
        self.backgroundColor = colorStyle.lightGray.withAlphaComponent(0.2)
    }
}
extension UITextView: Style {
    func style() {
        self.font = UIFont(name: "Helvetica", size: 16)
        self.textColor = colorStyle.gray
        self.backgroundColor = colorStyle.lightGray.withAlphaComponent(0.2)
        self.layer.borderColor = colorStyle.gray.cgColor
        self.layer.borderWidth = 1
    }
}
extension UIButton: Style {
    func style() {
        self.backgroundColor = colorStyle.red
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(colorStyle.black, for: .highlighted)
        self.layer.cornerRadius = self.layer.frame.height / 2
    }
}
