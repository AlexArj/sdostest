//
//  Colors.swift
//  SocialDrone
//
//  Created by Alejandro Arjonilla Garcia on 25.04.18.
//  Copyright © 2018 socialdrone. All rights reserved.
//

import Foundation
import UIKit

public struct ColorStyle {
    public var brown: UIColor { return #colorLiteral(red: 0.8078431373, green: 0.737254902, blue: 0.7019607843, alpha: 1)  }
    public var red: UIColor { return #colorLiteral(red: 0.9254901961, green: 0.3568627451, blue: 0.431372549, alpha: 1)  }
    public var gray: UIColor { return #colorLiteral(red: 0.8196078431, green: 0.8117647059, blue: 0.8156862745, alpha: 1)  }
    public var black: UIColor { return #colorLiteral(red: 0.4117647059, green: 0.4392156863, blue: 0.462745098, alpha: 1)  }
    public var lightGray: UIColor { return #colorLiteral(red: 0.8980392157, green: 0.862745098, blue: 0.8666666667, alpha: 1) }
}

public var colorStyle = ColorStyle()
