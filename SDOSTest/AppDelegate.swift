//
//  AppDelegate.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import UIKit
import Firebase
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var disposeBag: DisposeBag = DisposeBag()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        addUserStateListener()
        return true
    }
    private func addUserStateListener() {
        userState.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { (newState) in
            newState.saveState()
            let controller: UIViewController
            switch newState {
            case .logOut, .idle:
                controller = LoginRouter.createModule()
            case .loggedIn(let user):
                switch user.role! {
                case .admin:
                    controller = AdminRouter.createModule()
                case .tecnic:
                    controller = TecnicRouter.createModule()
                }
            }
            self.setRootController(controller: controller)
        }).disposed(by: disposeBag)
    }
    private func setRootController(controller: UIViewController) {
        self.window?.removeFromSuperview()
        self.window = nil
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let navController = UINavigationController.grayNavController
        navController.setViewControllers([controller], animated: true)
        self.window?.rootViewController = navController
        self.window?.makeKeyAndVisible()
    }
}

