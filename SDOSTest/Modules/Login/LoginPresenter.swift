//
//  LoginPresenter.swift
//  SDOSTest
//
//  Created Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit
import RxSwift

class LoginPresenter: LoginPresenterProtocol {
    
    weak private var view: LoginViewProtocol?
    var interactor: LoginInteractorProtocol?
    private let router: LoginWireframeProtocol
    var username: Variable<String>
    var password: Variable<String>
    var isLoading: Variable<Bool>
    
    init(interface: LoginViewProtocol, interactor: LoginInteractorProtocol?, router: LoginWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
        username = Variable("")
        password = Variable("")
        isLoading = Variable(false)
    }
    
    func login() {
        view?.addLoadingView()
        interactor?.login(with: username.value, password: password.value) { response in
            self.view?.removeLoadingView()
            switch response {
            case .error(let error):
                self.view?.showError(with: error)
            case .success(let role):
                switch role {
                case .admin:
                    self.router.navigateToAdmin()
                case .tecnic:
                    self.router.navigateToTecnic()
                }
            }
        }
    }
}
