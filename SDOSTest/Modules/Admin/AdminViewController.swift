//
//  AdminViewController.swift
//  SDOSTest
//
//  Created Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit
import RxSwift
import RxCocoa
import PMAlertController

class AdminViewController: BaseViewController, AdminViewProtocol {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var presenter: AdminPresenterProtocol?
    
    override func configureViews() {
        guard let presenter = self.presenter else { return }
        self.title = "Admin"
        descriptionTextView.addDoneButtonOnKeyboard()
        timeTextField.addDoneButtonOnKeyboard()
        bindViews()
        typeButton.add(for: .touchUpInside) { [unowned self] in
            self.view.endEditing(true)
            self.showTypeSelection()
        }
        saveButton.add(for: .touchUpInside) {
            self.view.endEditing(true)
            presenter.save()
        }
        configureNavBar()
    }
    private func configureNavBar() {
        guard let presenter = self.presenter else { return }
        self.title = UserState.user?.name
        let logOutButton = UIButton()
        logOutButton.setTitle("Log out", for: .normal)
        logOutButton.setTitleColor(colorStyle.red, for: .normal)
        logOutButton.add(for: .touchUpInside) {
            presenter.logout()
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: logOutButton)
    }
    func bindViews() {
        guard let presenter = self.presenter else { return }
        descriptionTextView.rx.didBeginEditing.subscribe { (_) in
            self.descriptionTextView.text = ""
            }.disposed(by: disposeBag)
        descriptionTextView.rx.text
            .orEmpty
            .bind(to: presenter.description)
            .disposed(by: disposeBag)
        timeTextField.rx.text
            .orEmpty
            .bind(to: presenter.time)
            .disposed(by: disposeBag)
        presenter.type.asObservable()
            .bind(to: typeButton.rx.title()).disposed(by: disposeBag)
        presenter.description.asObservable()
            .bind(to: descriptionTextView.rx.text).disposed(by: disposeBag)
        presenter.type.asObservable()
            .bind(to: typeButton.rx.title()).disposed(by: disposeBag)
        presenter.time.asObservable()
            .bind(to: timeTextField.rx.text).disposed(by: disposeBag)
        
    }
    func showTypeSelection() {
        
        let alertController = PMAlertController(title: "Select type", description: "",
                                                image: #imageLiteral(resourceName: "variety"), style: .alert)
        Task.TaskType.all.forEach { type in
            alertController.addAction(
                PMAlertAction(title: type.rawValue.uppercased(), style: .default,
                              action: { [unowned self] in
                                self.presenter?.setType(type)
                }))
        }
        let cancelAction = PMAlertAction(title: "Cancel", style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
