//
//  TecnicTableViewCell.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 24.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import UIKit

struct TecnicCellViewModel {
    let description: String?
    let state: Task.State?
    let tapAction: () -> Void
}
class TecnicTableViewCell: UITableViewCell {
    static var identifier: String {
        return "TecnicTableViewCell"
    }
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    override func awakeFromNib() {
        // TODO: - style cell please
        descriptionLabel.setTitleStyle()
        typeLabel.setSubTitleStyle()
    }
    func setup(_ viewModel: TecnicCellViewModel) {
        self.descriptionLabel.text = viewModel.description
        self.typeLabel.text = viewModel.state?.rawValue.localizedUppercase
        self.gestureRecognizers?.forEach {
            self.removeGestureRecognizer($0)
        }
        self.addTapGestureRecognizer(action: viewModel.tapAction)
        UIView.animate(withDuration: 0.5) {
            self.backgroundColor = viewModel.state?.color
        }
    }
}

extension Task.State {
    fileprivate var color: UIColor {
        switch self {
        case .finished: return #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 0.5)
        case .open: return #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 0.5)
        }
    }
}
