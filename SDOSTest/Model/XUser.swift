//
//  User.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import ObjectMapper

struct XUser: Mappable {
    enum Role: String {
        case admin, tecnic
    }
    var email: String?
    var id: String?
    var name: String?
    var role: Role?
    var tasksNumber: Int = 0
    var totalTimeAssigned: Double = 0

    init?(map: Map) {
        mapping(map: map)
    }
    mutating func mapping(map: Map) {
        email <- map["email"]
        id <- map["id"]
        name <- map["name"]
        role <- (map["role"],EnumTransform<Role>())
        tasksNumber <- map["tasksNumber"]
        totalTimeAssigned <- map["totalTimeAssigned"]
    }
}
