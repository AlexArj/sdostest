//
//  Task.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import ObjectMapper

struct Task: Mappable {
    enum State: String {
        case open, finished
    }
    enum TaskType: String {
        case repon, pay, wrap, etc
        static var all: [TaskType] = [.repon, .pay, .wrap, etc]
    }
    var description: String?
    var state: State?
    var time: Double?
    var type: TaskType?
    var userId: String?
    var id: String? = UUID().uuidString.appending("\(Date().ticks)")
    
    init?(map: Map) {
        mapping(map: map)
    }
    mutating func mapping(map: Map) {
        description <- map["description"]
        state <- (map["state"],EnumTransform<State>())
        time <- map["time"]
        type <- (map["type"],EnumTransform<TaskType>())
        userId <- map["userId"]
        id <- map["id"]
    }
    init(_ description: String, state: State = .open, time: Double, type: TaskType, userId: String = "") {
        self.description = description
        self.state = state
        self.time = time
        self.type = type
        self.userId = userId
    }
}
