//
//  UserState.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 23.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import RxSwift

var userState: Variable<UserState> = Variable<UserState>(UserState.loadCurrentState())

enum UserState: Equatable {
    case idle
    case loggedIn(XUser)
    case logOut
    
    private static var defaultKey: String {
        return "UserStateKey"
    }
    static func == (lhs: UserState, rhs: UserState) -> Bool {
        switch (lhs, rhs) {
        case (.idle, .idle),
             (.loggedIn, .loggedIn),
             (.logOut, .logOut): return true
        default: return false
        }
    }
    static func loadCurrentState() -> UserState {
        guard let dict = UserDefaults.standard.dictionary(forKey: defaultKey),
            let token = XUser(JSON: dict) else {
                return .idle
        }
        return .loggedIn(token)
    }
    func saveState() {
        var token: XUser? = nil
        switch self {
        case .loggedIn(let newToken):
            token = newToken
        default: break
        }
        UserDefaults.standard.set(token?.toJSON(), forKey: UserState.defaultKey)
    }
    static var user: XUser? {
        switch userState.value {
        case .loggedIn(let user): return user
        default: return nil
        }
    }
}
