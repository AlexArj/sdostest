//
//  BaseView.swift
//  SDOSTest
//
//  Created by Alejandro Arjonilla Garcia on 24.08.18.
//  Copyright © 2018 ArjonillaCo. All rights reserved.
//

import Foundation
import PMAlertController

struct AlertOption {
    let title: String?
    let action: () -> Void
}
protocol BaseView {
    func showAlert(with title: String, description: String, image: UIImage?, actions: [AlertOption])
    func showError(with error: Error)
    func addLoadingView()
    func removeLoadingView()
}
