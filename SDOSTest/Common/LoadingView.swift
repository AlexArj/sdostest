//
//  LoadingView.swift
//  SocialDrone
//
//  Created by Alejandro Arjonilla Garcia on 07.05.18.
//  Copyright © 2018 socialdrone. All rights reserved.
//

import Foundation
import UIKit

class LoadingView: UIView {
    var indicatorView: UIActivityIndicatorView!
    var tapEnabled: Bool = true
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    func setupView() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(blurEffectView)
        indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicatorView.tintColor = colorStyle.red
        addSubview(indicatorView)
        indicatorView.startAnimating()
        indicatorView.center = self.center
        bringSubview(toFront: indicatorView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        addGestureRecognizer(tap)
    }
    @objc func doubleTapped() {
        if tapEnabled { self.hideAnimated() }
    }
}
