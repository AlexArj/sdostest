//
//  BaseViewController.swift
//  SocialDrone
//
//  Created by Alejandro Arjonilla Garcia on 04.05.18.
//  Copyright © 2018 socialdrone. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

public class BaseViewController: UIViewController {
    internal var disposeBag: DisposeBag = DisposeBag()
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.view.backgroundColor = .white
            self.view.subviews
                .map { $0 as? Style}
                .forEach { $0?.style() }
            self.view.addTapGestureRecognizer {
                self.view.endEditing(true)
            }
        }
    }
    func configureViews() {}
}

extension BaseViewController {
    enum Route {
        case login
        
        var controller: UIViewController {
            switch self {
            case .login: return LoginViewController()
            }
        }
    }
    
    func show(route: Route) {
        self.navigationController?.show(route.controller, sender: nil)
    }
    func present(route: Route) {
        self.present(route.controller, animated: true, completion: nil)
    }
}
