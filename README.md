![Platform](https://img.shields.io/badge/platform-iOS-gray.svg)
# :bowtie: SDOS test project :bowtie:
<br/>![alt text](https://www.drupal.org/files/logoSDOS.png)
### Sample project usign VIPER.
Libraries used: Firebase, Nora, PMAlertController, Whisper, RxSwift, ObjectMapper, SnapKit
